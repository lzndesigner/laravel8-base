<?php

use Illuminate\Support\Facades\Route;
use App\Http\Livewire\{
    UsersShow
};

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('dashboard')->middleware(['auth:sanctum', 'verified'])->group(function () {

    Route::get('/', function () {
        return view('backend.dashboard');
    })->name('dashboard');

    Route::get('users', UsersShow::class)->name('users');
});



