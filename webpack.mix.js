const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/styles/backend/js/app_backend.js', 'public/js').sourceMaps();

mix.postCss('resources/styles/backend/css/custom_backend.css', 'public/css')
.postCss('resources/styles/backend/css/app_backend.css', 'public/css', [
    require('postcss-import'),
    require('tailwindcss'),
]);

mix.postCss('resources/styles/frontend/css/template_frontend.css', 'public/css');

mix.sass('resources/sass/fontawesome.scss', 'public/css');

if (mix.inProduction()) {
    mix.version();
}
