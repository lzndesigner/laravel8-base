<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;

class UsersShow extends Component
{
    use WithPagination;

    public $name, $email, $password, $user_id;
    public $showCreate = 0;
    public $showEdit = 0;

    public $min_paginate = 5;

    public function render()
    {
        $users = User::paginate($this->min_paginate);
        return view('backend.livewire.users-show', [
            'users' => $users
        ]);
    }

    private function resetCreateForm()
    {
        $this->name = '';
        $this->email = '';
        $this->password = '';
    }

    public function create()
    {
        $this->resetCreateForm();
        $this->openModalCreate();
    }

    public function openModalCreate()
    {
        $this->showCreate = true;
    }

    public function closeModalCreate()
    {
        $this->showCreate = false;
    }

    public function store()
    {
        $this->validate([
            'name' => 'required',
            'email' => 'required',
            'password' => 'required|min:8',
        ]);

        User::create([
            'name' => $this->name,
            'email' => $this->email,
            'password' => \Hash::make($this->password),
        ]);

        session()->flash('message', 'Usuário criado com sucesso!');

        $this->closeModalCreate();
        $this->resetCreateForm();
    }



    public function edit($id)
    {
        $user = User::findOrFail($id);
        $this->user_id = $id;
        $this->name = $user->name;
        $this->email = $user->email;
        if ($user->password) {
            $this->password = \Hash::make($user->password);
        }

        $this->openModalEdit();
        $this->password = '';
    }

    public function openModalEdit()
    {
        $this->showEdit = true;
    }

    public function closeModalEdit()
    {
        $this->showEdit = false;
    }

    public function update()
    {
        $user = User::findOrFail($this->user_id);

        $this->validate([
            'name' => 'required',
            'email' => 'required',
            'password' => 'min:8',
        ]);

        User::updateOrCreate(['id' => $this->user_id], [
            'name' => $this->name,
            'email' => $this->email,
            'password' => ($this->password ? (\Hash::check($this->password, $user->password) ? $user->password : \Hash::make($this->password)) : $user->password),
        ]);

        session()->flash('message', $this->user_id ? 'User updated.' : 'User created.');

        $this->closeModalEdit();
        $this->resetCreateForm();
    }



    public function delete($id)
    {
        User::find($id)->delete();
        session()->flash('message', 'Studen deleted.');
    }
}
