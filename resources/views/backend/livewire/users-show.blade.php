<div>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Usuários Administrativos
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white border overflow-hidden shadow-xl sm:rounded-lg px-4 py-4">
                @if (session()->has('message'))
                    <div class="bg-teal-100 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md my-3"
                        role="alert">
                        <div class="flex">
                            <div>
                                <p class="text-sm">{{ session('message') }}</p>
                            </div>
                        </div>
                    </div>
                @endif
                @if ($showCreate)
                    @include('backend.livewire.users-create')
                @elseif ($showEdit)
                    @include('backend.livewire.users-edit')
                @endif
                <table class="table-auto border w-full text-left">
                    <thead>
                        <tr class="bg-gray-100">
                            <th class="px-4 py-2 w-20">#</th>
                            <th class="px-4 py-2">Nome</th>
                            <th class="px-4 py-2">E-mail</th>
                            <th class="px-4 py-2 w-28 text-center">Ação</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                            <tr class="hover:bg-gray-50">
                                <td class="border px-4 py-2">{{ $user->id }}</td>
                                <td class="border px-4 py-2">{{ $user->name }}</td>
                                <td class="border px-4 py-2"><a href="mailto:{{ $user->email }}">{{ $user->email }}</a></td>
                                <td class="border px-4 py-2 w-28 text-center ">
                                    <button wire:click="edit({{ $user->id }})"
                                        class="bg-blue-500  text-white py-2 px-2 text-xs rounded"><i
                                            class="fa fa-edit"></i></button>
                                    <button wire:click="delete({{ $user->id }})"
                                        class="bg-red-500 hover:bg-red-700 text-white py-2 px-2 text-xs rounded"><i
                                            class="fa fa-trash"></i></button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <hr class="mt-4 mb-2">
                <div class="flex">
                    <div class="flex-initial px-3">                        
                        <button wire:click="create()" class="bg-green-600 text-white font-bold py-2 px-4 rounded my-3">
                            <i class="fa fa-plus"></i> Novo Usuário
                        </button>
                    </div>
                    <div class="flex-initial px-1 py-4">
                        <label for="min_paginate" class="w-full text-xs">Exibir:</label>
                        <select name="min_paginate" id="min_paginate" class="appearance-none border rounded text-xs text-gray-700 leading-tight focus:outline-none focus:shadow-outline" wire:model="min_paginate">
                            <option value="5">5</option>
                            <option value="10">10</option>
                            <option value="15">15</option>
                            <option value="20">20</option>
                        </select>
                    </div>
                    <div class="flex-auto">{{ $users->links() }}</div>
                </div>
                <hr class="m">
            </div>
        </div>
    </div>
</div>
