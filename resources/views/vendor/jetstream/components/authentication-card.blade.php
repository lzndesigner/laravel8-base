<div class="min-h-screen flex flex-col sm:justify-center items-center pt-6 sm:pt-0 bg-gray-100 bg-pattern-4">
    <div>
        {{ $logo }}
    </div>

    <div class="w-full sm:max-w-md mt-6 px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg">
        {{ $slot }}
    </div>

    <div class="py-4">
        <p class="text-sm">Desenvolvido por <a href="https://innsystem.com.br">InnSystem Inovação em Sistemas</a> {{ date('Y') }} ©</p>
    </div>
</div>
